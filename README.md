---

# K-Nearest Neighbor Estimation of Information Measures
## -- Matlab and Python Codes --
## Juan F. Restrepo
### <jrestrepo@ingenieria.uner.edu.ar>

_Laboratorio de Señales y Dinámicas no Lineales, Instituto de Bioingeniería y Bioinformática, CONICET - Universidad
Nacional de Entre Ríos. Ruta prov 11 km 10 Oro Verde, Entre Ríos, Argentina._

---

## Description

k-nearest neighbor estimation of Information Measures (Mutual Information and
Transfer Entropy).

The algorithms are based on the Kraskov-Stögbauer-Grassberger estimatior (first
and second algorithms) [1], and the modification introduced by
Gao-Steeg-Galstyan (vol correction) in [2].

**Refs:**
[1] [https://doi.org/10.1103/PhysRevE.69.066138](https://doi.org/10.1103/PhysRevE.69.066138)
[2] [https://arxiv.org/abs/1411.2003](https://arxiv.org/abs/1411.2003)
[3] The transfer entropy estimator for the first and second ksg-algorithm was found
   in [https://link.springer.com/chapter/10.1007/978-3-642-54474-3_1](https://link.springer.com/chapter/10.1007/978-3-642-54474-3_1).

---
