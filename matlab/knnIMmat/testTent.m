% Test ksg-algorithms with coupled henon map coupled Henon
clear;
clc;
m = 3;
nn = 10;
u = m;
tau = 1;
a = getAlpha(nn, 3*m);
snorm = true;

N = 3000;
cn = 21;
c = linspace(0, 0.95, cn);
Txy1 = zeros(cn,1);
Tyx1 = zeros(cn,1);
Txy2 = zeros(cn,1);
Tyx2 = zeros(cn,1);
for i = 1:cn
  x = CoupledHenon(N, c(i));
  Txy1(i) = knnTent01(x(:,1), x(:,2), m, tau, u, nn, a, snorm,1);
  Tyx1(i) = knnTent01(x(:,2), x(:,1), m, tau, u, nn, a, snorm,1);
  Txy2(i) = knnTent02(x(:,1), x(:,2), m, tau, u, nn, a, snorm);
  Tyx2(i) = knnTent02(x(:,2), x(:,1), m, tau, u, nn, a, snorm);
end

figure(1);
plot(c, Txy1 - Tyx1);
grid on;
title('Coupled Henon first ksg-algorithm');
xlabel('coupling');
ylabel('Txy - Tyx');
figure(2);
plot(c, Txy2 - Tyx2);
grid on;
title('Coupled Henon second ksg-algorithm');
xlabel('coupling');
ylabel('Txy - Tyx');


