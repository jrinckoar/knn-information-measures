function t = svdIndex(s)
    th = 99.9;
    cs = 100 * cumsum(s) / sum(s);
    t = find(cs >= th, 1, 'first');
end
