%{
test knnMutualInfo.m
test based on https://github.com/BiuBiuBiLL/NPEET_LNC

'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
2021-09-07
%}

%% Testing independence
nsamples = 500;
x = rand(nsamples, 1);
y = rand(nsamples, 1);
m = 1;
tau =1;
nn = 5;
alpha = getAlpha(nn, 2*m);
snorm = 0;
True_MI = 0;

LNC_MI1 = knnMI01(x, y, m, tau, nn, alpha, snorm,1);
LNC_MI2 = knnMI02(x, y, m, tau, nn, alpha, snorm);
disp(['LNC_MI_1=', num2str(LNC_MI1), ' ,True MI=', num2str(True_MI)]);
disp(['LNC_MI_2=', num2str(LNC_MI2), ' ,True MI=', num2str(True_MI)]);


%% Testing 2D linear relationship Y=X+Uniform_Noise
noise_level = 1e-7;
nsamples = 50000;
x = rand(nsamples, 1);
y = x + rand(nsamples, 1) * noise_level;

m = 1;
tau =1;
nn = 5;
alpha = getAlpha(nn, 2*m);
snorm = 0;
True_MI = 16.0472260191;

LNC_MI1 = knnMI01(x, y, m, tau, nn, alpha, snorm, 1);
LNC_MI2 = knnMI02(x, y, m, tau, nn, alpha, snorm);
disp(['LNC_MI_1=', num2str(LNC_MI1), ' ,True MI=', num2str(True_MI)]);
disp(['LNC_MI_2=', num2str(LNC_MI2), ' ,True MI=', num2str(True_MI)]);

%% Testing 2D quadratic relationship Y=X^2+Uniform_Noise
noise_level = 1e-7;
nsamples = 500;
x = rand(nsamples,1);
y = x.^2 + rand(nsamples,1) * noise_level;

m = 1;
tau =1;
nn = 5;
alpha = getAlpha(nn, 2*m);
snorm = 0;
True_MI = 15.8206637545;

LNC_MI1 = knnMI01(x, y, m, tau, nn, alpha,snorm);
LNC_MI2 = knnMI02(x, y, m, tau, nn, alpha, snorm);
disp(['LNC_MI_1=', num2str(LNC_MI1), ' ,True MI=', num2str(True_MI)]);
disp(['LNC_MI_2=', num2str(LNC_MI2), ' ,True MI=', num2str(True_MI)]);
