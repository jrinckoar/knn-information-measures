function [vol, vol_rec] = correctedVol(x)
    [nn, m] =  size(x);
    % center data
    x = (x - mean(x)) ./ sqrt(nn - 1);
    % SVD decomposition of centered vectors
    [~, s, v] = svd(x);
    t=svdIndex(diag(s));
    % t = min([svdIndex(diag(s)), nn, m]); % selecciona el rango de v
    y = x * v(:, 1:t);
    % calculate volume with respect to the mean value
    vol = sum(log(max(abs(x), [], 1)));
    vol_rec = sum(log(max(abs(y), [], 1)));
end
