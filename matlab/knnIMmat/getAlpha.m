function a = getAlpha(nn, m)
    eps = 5 * 1E-3;
    N = 5 * 1E5;
    vol = zeros(N, 1);
    vol_rec = zeros(N, 1);
    t = min(nn, m);

    parfor i = 1:N
        xr = rand(nn + 1, m);
        xr = (xr - mean(xr)) ./ sqrt(nn);
        [~, ~, v] = svd(xr);
        y = xr * v(:, 1:t);
        vol(i) = sum(log(max(abs(xr), [], 1)));
        vol_rec(i) = sum(log(max(abs(y), [], 1)));
    end

    ratio = sort(vol_rec - vol);
    a = exp(ratio(ceil(N * eps)));
end
