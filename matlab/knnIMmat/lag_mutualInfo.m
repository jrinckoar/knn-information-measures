function [I, tau]=lag_mutualInfo(x,y,tmax, nn, alpha)

tau = linspace(0, tmax, tmax + 1);
I = zeros(tmax+1, 1);
x = parallel.pool.Constant(x);
y = parallel.pool.Constant(y);
parfor i=1: tmax+1
    I(i) = kdtMutualInfo(x.Value(1:end-tau(i)), y.Value(tau(i)+1:end), nn, alpha);
end
