function I = knnMI02(x, y, m, tau, nn, alpha, snorm)
%kNNMI02 - k-nearest neighbor estimation of mutual information
%This algorithm is based on the Kraskov-Stögbauer-Grassberger estimatior
% (second algorithm) and the modification introduced by Gao-Steeg-Galstyan
% (vol correction).
%refs:
%1. https://doi.org/10.1103/PhysRevE.69.066138
%2. arXiv:1411.2003
%
% Syntax: I = knnMI02(x, y, m, tau, u, nn, alpha, snorm)
%
% Inputs:
%   x -- x signal
%   y -- y signal
%   m      -- Embedding dimension
%   tau    -- Embedding lag
%   nn     -- number of nearest neighbors
%   alpha  -- correction parameter for Gao's algorithm
%   snorm  -- if True normalize time series (cero mean and unitary variance)
%
% Outputs:
%   I -- Mutual information between x and y

% Author: Juan F. Restrepo, Ph.D.
% Work address
% email: jrestrepo@ingenieria.uner.edu.ar
% Website: http://www.
% Sep 2021; Last revision: 2021-10-19
% Copyright (c) 2021, Juan F. Restrepo
% All rights reserved.

metric = 'chebychev';
x = x(:);
y = y(:);

if snorm
    % series normlization (cero mean and unitary variance)
    x = (x - mean(x)) ./ std(x);
    y = (y - mean(y)) ./ std(y);
    % Add uniform noise
    x = x + rand(size(x)) * 1E-10;
    y = y + rand(size(y)) * 1E-10;
end

% Get delay vectors
xv = embedding(x, m, tau);
yv = embedding(y, m, tau);

% Form vector space vec = {Xm, Ym}
vec = [xv, yv];
[N, d] = size(vec);

% Find the knn in the complete space
[nn_inds, ~] = knnsearch(vec, vec, 'K', nn + 1, 'Distance', metric);

% Dists. in each marginal space
dist = zeros(N, d);
for i = 1:N
    dist(i, :) = max(abs(vec(nn_inds(i, :), :) - vec(i, :)), [], 1);
end
dist = dist - 1E-15;

% find k-nearest neighbor in marginal spaces
nn1 = zeros(N, d);
for j = 1:d
    Mdl = KDTreeSearcher(vec(:, j), 'Distance', metric);
    for i = 1:N
        w = rangesearch(Mdl, vec(i, j), dist(i, j), 'Distance', metric);
        nn1(i, j) = length(w{1});
    end
end

% Calculate ksg estimator (algorithm 2)
Iksg = psi(nn) - (d - 1) / nn + (d - 1) * psi(N) - sum(mean(psi(nn1), 1));

% Calculate volume of hiper-rectangle (second ksg-alg) and volume correction
vol = zeros(N,1);
vol_rec = zeros(N,1);
for i =1:N
  [vol(i), vol_rec(i)] = correctedVol(vec(nn_inds(i,:), :));
end

inds = vol_rec >= vol + log(alpha);
vol_rec(inds)= vol(inds);
e = mean(vol - vol_rec);

% Corrected mutual information
I = Iksg + e;
