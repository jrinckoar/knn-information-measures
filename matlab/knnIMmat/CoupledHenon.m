function x = CoupledHenon(N, c)
%COUPLEDHENON Simulates the Henon-Henon coupled system.
% System X1 drives system X2
% For reference see: https://arxiv.org/abs/1903.07720
%
%
% Syntax: x = CoupledHenon(N, e)
%
% Inputs:
%   N -- Length of the time series
%   c -- Coupling parameter
%
% Outputs:
%   x -- x[:, 1] drives x[:, 2] 
%
% $Date: 2021-09-20
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.

n0 = 1000;
N = N + n0 - 1;
x = zeros(N, 2);
% Random initial contion
x(1:2, 1:2) = randn(2,2)*0.1;

flag = 1;
while flag
    for i=3: N
        x(i, 1) = 1.4 - x(i-1, 1)^2 + 0.3 * x(i-2, 1);
        x(i, 2) = 1.4 - (c * x(i-1, 1) + (1-c) * x(i-1, 2)) * x(i-1, 2) ...
            + 0.3 * x(i-2, 2);
    end
    if sum(isinf(x)) > 0    % check if solutions are bounded
        disp('inf')
        x(1:2, 1:2) = randn(2,2) * 0.1;
    else
        flag = 0;
    end
end
% Remove the 10000 first data points
x = x(n0 + 1:end, :);
