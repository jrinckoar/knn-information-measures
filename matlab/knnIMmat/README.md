---

# K-Nearest Neighbor Estimation of Information Measures
## -- Matlab --
## Juan F. Restrepo
### <jrestrepo@ingenieria.uner.edu.ar>

_Laboratorio de Señales y Dinámicas no Lineales, Instituto de Bioingeniería y Bioinformática, CONICET - Universidad
Nacional de Entre Ríos. Ruta prov 11 km 10 Oro Verde, Entre Ríos, Argentina._

---

## Files
1.
