function V = delayVectors(x, m, tau)

[n, nseries] = size(x);
k = (m-1) * tau;
L = n - k;
V = zeros(L, m * nseries);
for i=1:L
    v = reshape(x(i:tau:i+k, :), 1, []);
    V(i, :)=v;
end
