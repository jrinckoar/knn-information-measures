function T = knnTent01(source, target, m, tau, u, nn, alpha, snorm, vcor)
%kNNTENT01 - k-nearest neighbor estimation of transfer entropy
%This algorithm is based on the Kraskov-Stögbauer-Grassberger estimatior (first algorithm)
%and the modification introduced by Gao-Steeg-Galstyan (vol correction).
%refs:
%1. https://doi.org/10.1103/PhysRevE.69.066138
%2. arXiv:1411.2003
%3. The transfer entropy estimator for the FIRST ksg-algorithm was found
%   in DOI 10.1007/978-3-642-54474-3_1.
%
% Syntax: T = knnTent01(source, target, m, tau, u, nn, alpha, snorm)
%
% Inputs:
%   source -- Source signal
%   target -- Target signal
%   m      -- Embedding dimension
%   tau    -- Embedding lag [tau_source, tau_target] or just [tau]
%   u      -- time index ahead for the target signal
%   nn     -- number of nearest neighbors
%   alpha  -- correction parameter for Gao's algorithm
%   snorm  -- if True normalize time series (cero mean and unitary variance)
%   vcor   -- if True volume correction
%
% Outputs:
%   T -- Transfer entropy from source to target

% Author: Juan F. Restrepo, Ph.D.
% Work address
% email: jrestrepo@ingenieria.uner.edu.ar
% Website: http://www.
% Sep 2021; Last revision: 2021-09-23
% Copyright (c) 2021, Juan F. Restrepo
% All rights reserved.

metric = 'chebychev';
source = source(:);
target = target(:);

if snorm
    % series normlization (cero mean and unitary variance)
    source = (source - mean(source)) ./ std(source);
    target = (target - mean(target)) ./ std(target);
    % Add uniform noise
    source = source + rand(size(source)) * 1E-10;
    target = target + rand(size(target)) * 1E-10;
end

% Get delay vectors
if length(tau) == 2
    taus = tau(1);
    taut = tau(2);
else
    taus = tau;
    taut = tau;
end

sv = embedding(source, m, taus);
tv = embedding(target, m, taut);
N = min([length(sv), length(tv)]);
sv = sv(1:N,:);
tv = tv(1:N,:);
u = u + 1;

% Form vector space vec = {Target_(t+u), Target_t, Source_t}
vec = [tv(u:end, :), tv(1:end-u+1,:), sv(1:end-u+1,:)];
N = length(vec);
% Find the knn in the complete space
[nn_inds, ~]= knnsearch(vec, vec, 'K', nn + 1, 'Distance', metric);

% Dists. in complete space {Target_(t+u), Target_t, Source_t}
dist = zeros(N, 1);
for i=1:N
    dist(i) = max(max(abs(vec(nn_inds(i,:),:) - vec(i,:)),[], 1));
end
dist = dist - 1E-15;

% Num. of nearest neighbors in space {Target_t}
nnT = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, m+1:2*m), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, m+1:2*m), dist(i), 'Distance', metric);
    nnT(i) = length(w{1});
end
% Num. of nearest neighbors in space {Target_(t+u), Target_t}
nnTu = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, 1:2*m), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, 1:2*m), dist(i), 'Distance', metric);
    nnTu(i) = length(w{1});
end
% Num. of nearest neighbors in space {Target_t, Source_t}
nnTS = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, m+1:end), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, m+1:end), dist(i), 'Distance', metric);
    nnTS(i) = length(w{1});
end

% Calculate ksg estimator (algorithm 1)
T = psi(nn) + mean(psi(nnT + 1)) - mean(psi(nnTu + 1)) ...
    - mean(psi(nnTS + 1));

if vcor
    % Calculate volume of hiper-cube (first ksg-alg) and volume correction
    vol = zeros(N,1);
    vol_rec = zeros(N,1);
    for i =1:N
      [vol(i), vol_rec(i)] = correctedVol(vec(nn_inds(i,:), :));
    end

    inds = vol_rec >= vol + log(alpha);
    vol_rec(inds)= vol(inds);
    e = mean(vol - vol_rec);

    % Corrected transfer entropy
    T = T + e;
end
