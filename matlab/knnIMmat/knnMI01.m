function I = knnMI01(x, y, m, tau, nn, alpha, snorm,vcor)
%kNNMI01 - k-nearest neighbor estimation of mutual information
%This algorithm is based on the Kraskov-Stögbauer-Grassberger estimatior (first algorithm)
%and the modification introduced by Gao-Steeg-Galstyan (vol correction).
%refs:
%1. https://doi.org/10.1103/PhysRevE.69.066138
%2. arXiv:1411.2003
%
% Syntax: I = knn_tent01(x, y, m, tau, u, nn, alpha)
%
% Inputs:
%   x -- x signal
%   y -- y signal
%   m      -- Embedding dimension
%   tau    -- Embedding lag
%   nn     -- number of nearest neighbors
%   alpha  -- correction parameter for Gao's algorithm
%   snorm  -- if True normalize time series (cero mean and unitary variance)
%
% Outputs:
%   I -- Mutual information between x and y

% Author: Juan F. Restrepo, Ph.D.
% Work address
% email: jrestrepo@ingenieria.uner.edu.ar
% Website: http://www.
% Sep 2021; Last revision: 2021-10-19
% Copyright (c) 2021, Juan F. Restrepo
% All rights reserved.

metric = 'chebychev';
x = x(:);
y = y(:);

if snorm
    % series normlization (cero mean and unitary variance)
    x = (x - mean(x)) ./ std(x);
    y = (y - mean(y)) ./ std(y);
    % Add uniform noise
    x = x + rand(size(x)) * 1E-10;
    y = y + rand(size(y)) * 1E-10;
end

% Get delay vectors
xv = embedding(x, m, tau);
yv = embedding(y, m, tau);

% Form vector space vec = {Xm, Ym}
vec = [xv, yv];
N = length(vec);

% Find the knn in the complete space
[nn_inds, ~] = knnsearch(vec, vec, 'K', nn + 1, 'Distance', metric);

% Dists. in complete space {xv, yv}
dist = zeros(N, 1);
for i = 1:N
    dist(i) = max(max(abs(vec(nn_inds(i, :), :) - vec(i, :)), [], 1));
end
dist = dist - 1E-15;

% Num. of nearest neighbors in space {xv}
nnX = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, 1:m), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, 1:m), dist(i), 'Distance', metric);
    nnX(i) = length(w{1});
end

% Num. of nearest neighbors in space {yv}
nnY = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, m+1:end), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, m+1:end), dist(i), 'Distance', metric);
    nnY(i) = length(w{1});
end

% Calculate ksg estimator (algorithm 1)
I = psi(nn) + psi(N) - mean(psi(nnX + 1)) - mean(psi(nnY + 1));

% % Kraskov-Stögbauer-Grassberger estimatior
% Iksg = psi(nn) - (d - 1) / nn + (d - 1) * psi(N) - sum(mean(psi(nn1), 1));

% Calculate volume of hiper-rectangle (second ksg-alg) and volume correction
%vol = zeros(N,1);
%vol_rec = zeros(N,1);
%for i =1:N
 % [vol(i), vol_rec(i)] = correctedVol(vec(nn_inds(i,:), :));
%end

%inds = vol_rec >= vol + log(alpha);
%vol_rec(inds)= vol(inds);
%e = mean(vol - vol_rec);

if vcor
    % Calculate volume of hiper-cube (first ksg-alg) and volume correction
    vol = zeros(N,1);
    vol_rec = zeros(N,1);
    for i =1:N
      [vol(i), vol_rec(i)] = correctedVol(vec(nn_inds(i,:), :));
    end

    inds = vol_rec >= vol + log(alpha);
    vol_rec(inds)= vol(inds);
    e = mean(vol - vol_rec);

    % Corrected transfer entropy
    I = I + e;
end

% Corrected mutual information
%I = Iksg + e;
