function T = knnTent02(source, target, m, tau, u, nn, alpha, snorm)
%kNNTENT02 - k-nearest neighbor estimation of transfer entropy
%This algorithm is based on the Kraskov-Stögbauer-Grassberger estimatior (second algorithm)
%and the modification introduced by Gao-Steeg-Galstyan (vol correction).
%refs:
%1. https://doi.org/10.1103/PhysRevE.69.066138
%2. arXiv:1411.2003
%3. The transfer entropy estimator for the SECOND ksg-algorithm was found
%   in DOI 10.1007/978-3-642-54474-3_1.
%
%
% Syntax: T = knnTent02(source, target, m, tau, u, nn, alpha)
%
% Inputs:
%   source -- Source signal
%   target -- Target signal
%   m      -- Embedding dimension
%   tau    -- Embedding lag
%   u      -- time index ahead for the target signal
%   nn     -- number of nearest neighbors
%   alpha  -- correction parameter for Gao's algorithm
%   snorm  -- if True normalize time series (cero mean and unitary variance)
%
% Outputs:
%   T -- Transfer entropy from source to target

% Author: Juan F. Restrepo, Ph.D.
% Work address
% email: jrestrepo@ingenieria.uner.edu.ar
% Website: http://www.
% Sep 2021; Last revision: 2021-10-19
% Copyright (c) 2021, Juan F. Restrepo
% All rights reserved.

algorithm = 2;
metric = 'chebychev';
source = source(:);
target = target(:);

if snorm
    % series normlization (cero mean and unitary variance)
    source = (source - mean(source)) ./ std(source);
    target = (target - mean(target)) ./ std(target);
    % Add uniform noise
    source = source + rand(size(source)) * 1E-10;
    target = target + rand(size(target)) * 1E-10;
end
% Get delay vectors
sv = embedding(source, m, tau);
tv = embedding(target, m, tau);
u = u + 1;

% Form vector space vec = {Target_(t+u), Target_t, Source_t}
vec = [tv(u:end, :), tv(1:end-u+1,:), sv(1:end-u+1,:)];

% Find the knn in the complete space
[nn_inds, ~]= knnsearch(vec, vec, 'K', nn + 1, 'Distance', metric);

% Find distances in each marginal space
N = length(vec);
dist1 = zeros(N, 1);
dist2 = zeros(N, 1);
dist3 = zeros(N, 1);
for i=1:N
    dt = max(abs(vec(nn_inds(i,:),:) - vec(i,:)),[], 1);
    % Dists. in the marginal space {Target_t}
    dist1(i) = max(dt(:, m+1:2*m));
    % Dists. in the marginal space {Target_(t+u), Target_t}
    dist2(i) = max(dt(:, 1:2*m));
    % Dists. in the marginal space {Target_t, Source_t}
    dist3(i) = max(dt(:, m+1:end));
end
dist1 = dist1 - 1E-15;
dist2 = dist2 - 1E-15;
dist3 = dist3 - 1E-15;

% Num. of nearest neighbors in space {Target_t}
nnT = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, m+1:2*m), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, m+1:2*m), dist1(i), 'Distance', metric);
    nnT(i) = length(w{1});
end
% Num. of nearest neighbors in space {Target_(t+u), Target_t}
nnTu = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, 1:2*m), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, 1:2*m), dist2(i), 'Distance', metric);
    nnTu(i) = length(w{1});
end
% Num. of nearest neighbors in space {Target_t, Source_t}
nnTS = zeros(N, 1);
Mdl = KDTreeSearcher(vec(:, m+1:end), 'Distance', metric);
for i =1:N
    w = rangesearch(Mdl, vec(i, m+1:end), dist3(i), 'Distance', metric);
    nnTS(i) = length(w{1});
end

% Calculate ksg estimator (algorithm 2)
Tksg = psi(nn) - 2/nn + mean(psi(nnT)) - mean(psi(nnTu)) - mean(psi(nnTS))...
    + mean(1./nnTu) + mean(1./nnTS);

% Calculate volume of hiper-rectangle (second ksg-alg) and volume correction
vol = zeros(N,1);
vol_rec = zeros(N,1);
for i =1:N
  [vol(i), vol_rec(i)] = correctedVol(vec(nn_inds(i,:), :));
end

inds = vol_rec >= vol + log(alpha);
vol_rec(inds)= vol(inds);
e = mean(vol - vol_rec);

% Corrected transfer entropy
T = Tksg + e;
