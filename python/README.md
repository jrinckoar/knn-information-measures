---

# K-Nearest Neighbor Estimation of Information Measures
## -- Python --
## Juan F. Restrepo
### <jrestrepo@ingenieria.uner.edu.ar>

Laboratorio de Señales y Dinámicas no Lineales, Instituto de Bioingeniería y Bioinformática, CONICET - Universidad
Nacional de Entre Ríos. Ruta prov 11 km 10 Oro Verde, Entre Ríos, Argentina.

---

## Install
```
cd /knn-information-measures/python
pip install -e .
```

## Files
1. knnTent.py: Transfer entropy. 
2. knnMI.py: Mutual information.
3. knnTools.py: Utility functions.

## Usage
### Mutual information
```
import numpy as np
from matplotlib import pyplot as plt

from knnIMpy.knnMI import knnMI01, knnMI02
from knnIMpy.knnTools import getAlpha

mu = np.array([0, 0])
Rho = np.linspace(-0.99, 0.99, 30)
nsamples = 3000
# a = 0.25
a = getAlpha(5, 2)

LNC_MI01 = []
LNC_MI02 = []
trueMI = []
for rho in Rho:
    cov_matrix = np.array([[1, rho], [rho, 1]])
    joint_samples = np.random.multivariate_normal(mean=mu,
                                                  cov=cov_matrix,
                                                  size=(nsamples, 1))
    trueMI.append(-0.5 * np.log(1 - rho**2))
    X = np.squeeze(joint_samples[:, :, 0])
    Y = np.squeeze(joint_samples[:, :, 1])
    LNC_MI01.append(knnMI01(X, Y, m=1, tau=1, nn=5, alpha=a, vcor=True))
    LNC_MI02.append(knnMI02(X, Y, m=1, tau=1, nn=5, alpha=a, vcor=True))

fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
ax.plot(Rho, trueMI, '.k', label='True')
ax.plot(Rho, LNC_MI01, 'b', label='Alg 1')
ax.legend(loc='upper center')
ax.plot(Rho, LNC_MI02, 'r', label='Alg 2')
ax.set_xlabel("rho")
ax.set_ylabel("IM")
ax.legend(loc='upper center')
plt.show()
```

### Transfer entropy
```
import numpy as np
from matplotlib import pyplot as plt

from knnIMpy.knnTent import knnTent01, knnTent02
from knnIMpy.knnTools import coupledHenon as henon
from knnIMpy.knnTools import getAlpha

m = 3
nn = 8
a = getAlpha(nn, 3 * m)
# a = 0.01136
opt = {'m': m, 'tau': [1, 2], 'nn': nn, 'u': 1, 'alpha': a, 'vcor': True}
n = 3000
c = np.linspace(0, 0.9, 31)

Txy1 = []
Tyx1 = []
Txy2 = []
Tyx2 = []
for c0 in c:
    x = henon(n, c0)
    Txy1.append(knnTent01(x[:, 0], x[:, 1], **opt))
    Tyx1.append(knnTent01(x[:, 1], x[:, 0], **opt))
    Txy2.append(knnTent02(x[:, 0], x[:, 1], **opt))
    Tyx2.append(knnTent02(x[:, 1], x[:, 0], **opt))

Txy1 = np.array(Txy1)
Tyx1 = np.array(Tyx1)
Txy2 = np.array(Txy2)
Tyx2 = np.array(Tyx2)

fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True)
ax1.plot(c, Txy1 - Tyx1)
ax1.set_title("Henon - Ksg algorithm 1")
ax1.set_xlabel("coupling")
ax1.set_ylabel("Txy - Tyx")
ax2.plot(c, Txy2 - Tyx2)
ax2.set_title("Henon - Ksg algorithm 2")
ax2.set_xlabel("coupling")
plt.show()
```
