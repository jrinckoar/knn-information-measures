#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="knnIMpy",
    version="0.1",
    description="knn information measures",
    ong_description=long_description,
    long_description_content_type="text/markdown",
    url="https://jrinckoar@bitbucket.org/jrinckoar/" +
    "knn-information-measures/" + "src/master/knnIMpy/",
    author="Juan F. Restrepo",
    author_email="jrestrepo@ingenieria.uner.edu.ar",
    license="MIT",
    # packages=["knnIMpy"],
    packages=find_packages(include=["knnIMpy", "knnIMpy.*"]),
    keywords="Mutual-information Transfer-entropy knn",
    install_requires=[
        "sklearn", "scipy", "numpy", "matplotlib", "ray", "psutil"
    ],
    test_suite="nose.collector",
    tests_require=["nose", "nose-cover3"],
    zip_safe=False,
)
