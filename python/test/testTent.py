"""
test knn_Tent

knn-Transfer Entropy

'Juan Felipe Restrepo <juan.restrepoo@uner.edu.ar>'
"""

import numpy as np
from matplotlib import pyplot as plt

from knnIMpy.knnTent import knnTent01, knnTent02
from knnIMpy.knnTools import coupledHenon as henon
from knnIMpy.knnTools import getAlpha

m = 3
nn = 8
a = getAlpha(nn, 3 * m)
# a = 0.01136
opt = {'m': m, 'tau': [1, 2], 'nn': nn, 'u': 1, 'alpha': a, 'vcor': True}
n = 3000
c = np.linspace(0, 0.9, 21)

Txy1 = []
Tyx1 = []
Txy2 = []
Tyx2 = []
for c0 in c:
    x = henon(n, c0)
    Txy1.append(knnTent01(x[:, 0], x[:, 1], **opt))
    Tyx1.append(knnTent01(x[:, 1], x[:, 0], **opt))
    Txy2.append(knnTent02(x[:, 0], x[:, 1], **opt))
    Tyx2.append(knnTent02(x[:, 1], x[:, 0], **opt))

Txy1 = np.array(Txy1)
Tyx1 = np.array(Tyx1)
Txy2 = np.array(Txy2)
Tyx2 = np.array(Tyx2)

fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True)
ax1.plot(c, Txy1 - Tyx1)
ax1.set_title("Henon - Ksg algorithm 1")
ax1.set_xlabel("coupling")
ax1.set_ylabel("Txy - Tyx")
ax2.plot(c, Txy2 - Tyx2)
ax2.set_title("Henon - Ksg algorithm 2")
ax2.set_xlabel("coupling")
plt.show()
