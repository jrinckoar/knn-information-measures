"""
test knn_MI

KNN MUTUAL INFORMATION

'Juan Felipe Restrepo <juan.restrepoo@uner.edu.ar>'
"""

import numpy as np
from matplotlib import pyplot as plt

from knnIMpy.knnMI import knnMI01, knnMI02
from knnIMpy.knnTools import getAlpha

mu = np.array([0, 0])
Rho = np.linspace(-0.99, 0.99, 30)
nsamples = 3000
# a = 0.25
a = getAlpha(5, 2)

LNC_MI01 = []
LNC_MI02 = []
trueMI = []
for rho in Rho:
    cov_matrix = np.array([[1, rho], [rho, 1]])
    joint_samples = np.random.multivariate_normal(mean=mu,
                                                  cov=cov_matrix,
                                                  size=(nsamples, 1))
    trueMI.append(-0.5 * np.log(1 - rho**2))
    X = np.squeeze(joint_samples[:, :, 0])
    Y = np.squeeze(joint_samples[:, :, 1])
    LNC_MI01.append(knnMI01(X, Y, m=1, tau=1, nn=5, alpha=a, vcor=True))
    LNC_MI02.append(knnMI02(X, Y, m=1, tau=1, nn=5, alpha=a, vcor=True))

fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
ax.plot(Rho, trueMI, '.k', label='True')
ax.plot(Rho, LNC_MI01, 'b', label='Alg 1')
ax.legend(loc='upper center')
ax.plot(Rho, LNC_MI02, 'r', label='Alg 2')
ax.set_xlabel("rho")
ax.set_ylabel("IM")
ax.legend(loc='upper center')
plt.show()
