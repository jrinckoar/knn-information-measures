"""
knn_mutualInfo
k-nearest neighbor estimation of mutual information.

This algorithm is  based  on  the  Kraskov-Stögbauer-Grassberger  estimatior (first and
second  algorithms)   and  the  modification  introduced   by  Gao-Steeg-Galstyan  (vol
correction).

The volumen corecction was proposed for the 2nd KSG-algorithm, it is a mystery if there
is a  someting similar for  the 1st KSG-algorithm.  I  use the corecction  also for the
first KSG-algorithm.

refs:
    1. https://doi.org/10.1103/PhysRevE.69.066138
    2. arXiv:1411.2003
    3. https://github.com/BiuBiuBiLL/NPEET_LNC

'Juan Felipe Restrepo <juan.restrepo@uner.edu.ar>'
"""
import numpy as np
from scipy.special import psi
from sklearn.neighbors import KDTree

try:
    from . import knnTools as knnT
except (ImportError):
    import knnTools as knnT


def knnMI01(x, y, m=2, tau=1, nn=8, alpha=0.01, vcor=False):

    metric = "infinity"
    # series normalization adding some noise
    x = knnT.series_normalization(x)
    y = knnT.series_normalization(y)

    # delay vectors
    xv = knnT.embedding(x, m, tau)
    yv = knnT.embedding(y, m, tau)

    # Form vector space vec = {Xm, Ym}
    vec = np.concatenate((xv, yv), axis=1)
    N, d = vec.shape

    # Find the knn in the complete space
    tree = KDTree(vec, metric=metric)
    nn_inds = tree.query(vec,
                         k=nn + 1,
                         return_distance=False,
                         sort_results=False)

    # Get distances to k-nearest neighbor and volume
    dist = np.zeros(N)
    # vol = np.zeros(N)
    for i in range(N):
        # dist[i] = np.abs(vec[nn_inds[i], :] - vec[i, :]).max()
        # vol[i] = np.log(dist[i])  # Vol for alg 1.
        dt = np.abs(vec[nn_inds[i], :] - vec[i, :]).max(axis=0)
        dist[i] = dt.max()  # distance for 1st-KSG-alg.
        # vol[i] = np.log(dt).sum()  # Vol for 2nd KSG-alg.

    # find k-nearest neighbor in proyected dimensions
    dist = dist - 1e-15
    nn1 = np.zeros((N, d))
    for j in range(d):
        tree = KDTree(vec[:, j].reshape(-1, 1), metric=metric)
        nn1[:, j] = tree.query_radius(vec[:, j].reshape(-1, 1),
                                      dist,
                                      return_distance=False,
                                      count_only=True)

    # Kraskov-Stögbauer-Grassberger estimatior 1st alg
    Iksg = psi(nn) + (d - 1) * psi(N) - psi(nn1).mean(axis=0).sum()

    # Volume correction
    if vcor:
        vol = np.zeros(N)
        vol_rec = np.zeros(N)
        for i in range(N):
            vol[i], vol_rec[i] = knnT.correctVolume(vec[nn_inds[i], :], nn)

        c_inds = vol_rec > vol + np.log(alpha)
        vol_rec[c_inds] = vol[c_inds]
        e = np.mean(vol - vol_rec)
        Iksg = Iksg + e

    return Iksg


def knnMI02(x, y, m=2, tau=1, nn=8, alpha=0.01, vcor=False):

    metric = "infinity"
    # series normalization adding some noise
    x = knnT.series_normalization(x)
    y = knnT.series_normalization(y)

    # delay vectors
    xv = knnT.embedding(x, m, tau)
    yv = knnT.embedding(y, m, tau)

    # Form vector space vec = {Xm, Ym}
    vec = np.concatenate((xv, yv), axis=1)
    N, d = vec.shape

    # Find the knn in the complete space
    tree = KDTree(vec, metric=metric)
    nn_inds = tree.query(vec,
                         k=nn + 1,
                         return_distance=False,
                         sort_results=False)

    # Get distances to k-nearest neighbor and volume
    dist = np.zeros((N, d))
    vol = np.zeros(N)
    for i in range(N):
        dt = np.abs(vec[nn_inds[i], :] - vec[i, :]).max(axis=0)
        dist[i, :] = dt  # distance for 2nd-KSG-alg.
        vol[i] = np.log(dt).sum()  # Vol for alg 2.

    # find nearest neighbor in proyected dimensions
    dist = dist - 1e-15
    nn1 = np.zeros((N, d))
    for j in range(d):
        tree = KDTree(vec[:, j].reshape(-1, 1), metric=metric)
        nn1[:, j] = tree.query_radius(vec[:, j].reshape(-1, 1),
                                      dist[:, j],
                                      return_distance=False,
                                      count_only=True)
    # Kraskov-Stögbauer-Grassberger estimatior 2nd alg
    Iksg = psi(nn) - (d - 1) / nn + (d - 1) * psi(N) - psi(nn1).mean(
        axis=0).sum()

    # Volume correction
    if vcor:
        vol = np.zeros(N)
        vol_rec = np.zeros(N)
        for i in range(N):
            vol[i], vol_rec[i] = knnT.correctVolume(vec[nn_inds[i], :], nn)

        c_inds = vol_rec > vol + np.log(alpha)
        vol_rec[c_inds] = vol[c_inds]
        e = np.mean(vol - vol_rec)
        Iksg = Iksg + e

    return Iksg
