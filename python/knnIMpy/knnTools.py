"""
Knn tools

"""

import numpy as np
import psutil
import ray
from numpy.linalg import svd

if not ray.is_initialized():
    num_cpus = psutil.cpu_count()
    ray.init(num_cpus=num_cpus)

# Series normalization --------------------------------------------------------


def toColVector(x):
    """
    Change vectors to colum vectors
    """
    x = x.reshape(x.shape[0], -1)
    if x.shape[0] < x.shape[1]:
        x = x.T
    return x


def series_normalization(x):
    """
    Add normal noise  to the series and normalize each  column to cero mean
    and unitary variance
    """
    x = toColVector(x)
    x = (x - np.mean(x, axis=0)) / np.std(x, axis=0)
    x = x + 1e-10 * np.random.rand(*x.shape)
    return x


# Embedding -------------------------------------------------------------------


def parseTau(tau):

    if type(tau) is not list:
        taus = taut = tau
    else:
        taus = tau[0]
        taut = tau[1]
    return int(taus), int(taut)


def embedding(x, m, tau):
    """
    Time series embedding
    """
    x = toColVector(x)
    n, k = x.shape
    l_ = n - (m - 1) * tau
    V = np.zeros((l_, m * k))

    for j in range(k):
        for i in range(m):
            V[:, i + j * m] = x[i * tau:i * tau + l_, j]
    return V


# knn algorithm ---------------------------------------------------------------


def checkSingularValues(s):
    th = 99.9
    cs = 100 * np.cumsum(s) / s.sum()
    inds = np.where(cs >= th)
    return inds[0][0]


# @ray.remote
def correctVolume(x, nn):
    nn, _ = x.shape
    # center data
    x = (x - x.mean(axis=0)) / np.sqrt(nn - 1)
    # SVD decomposition of centered vectors
    _, s, vh = np.linalg.svd(x)
    t = checkSingularValues(s)
    y = x @ vh.T[:, :t]
    vol = np.log(np.abs(x).max(axis=0)).sum()
    vol_rec = np.log(np.abs(y).max(axis=0)).sum()
    return vol, vol_rec


def getAlpha(nn, m, eps=5e-3, n=5e5):
    n = int(n)
    vol, vol_rec, ray_ids = [], [], []

    for _ in range(n):
        ray_ids.append(_get_alpha.remote(nn, m))

    for ray_id in ray_ids:
        vol.append(ray.get(ray_id)[0])
        vol_rec.append(ray.get(ray_id)[1])

    vol = np.array(vol)
    vol_rec = np.array(vol_rec)

    ratio = np.sort(vol_rec - vol)
    return np.exp(ratio[int(np.ceil(n * eps))])


@ray.remote
def _get_alpha(nn, m):
    xr = np.random.rand(nn + 1, m)
    # xr = xr - xr[0, :]
    xr = (xr - xr.mean(axis=0)) / np.sqrt(nn)
    _, s, vh = svd(xr)
    t = checkSingularValues(s)
    yr = xr @ vh.T[:, :t]
    vol = np.log(np.abs(xr).max(axis=0)).sum()
    vol_rec = np.log(np.abs(yr).max(axis=0)).sum()
    return (vol, vol_rec)


# dynamical systems -----------------------------------------------------------


def coupledHenon(n, c):
    """Coupled Henon map X1->X2"""
    n0 = 1000
    n = n + n0
    x = np.zeros((n, 2))
    x[0:2, :] = np.random.rand(2, 2)
    for i in range(2, n):
        x[i, 0] = 1.4 - x[i - 1, 0]**2 + 0.3 * x[i - 2, 0]

        x[i, 1] = (1.4 - c * x[i - 1, 0] * x[i - 1, 1] -
                   (1 - c) * x[i - 1, 1]**2 + 0.3 * x[i - 2, 1])

    return x[n0:, :]


if __name__ == "__main__":
    nn = 15
    m = 4
    a = getAlpha(nn, m)
    print(a)
    # # a = np.zeros((30, 1))
    # # # for i in range(30):
    # #     a[i] = get_alpha(nn, m)
    # #     print(i)
    # # plt.hist(a, 32)
    # # plt.show()
    # n = 500
    # x = coupledHenon(n, 0.5)
    # _, ax = plt.subplots(2)
    # ax[0].plot(x[:-1, 0], x[1:, 0], ".")
    # ax[1].plot(x[:-1, 1], x[1:, 1], ".")
    # plt.show()
