"""
KNNTent k-nearest neighbor estimation of transfer entropy
 This  algorithm  is  based  on  the  Kraskov-Stögbauer-Grassberger  estimatiors (first
 algorithm) and the modification introduced by Gao-Steeg-Galstyan (vol correction).

Parameters:
    source: source signal.
    target: target signal.
    m: embedding dimension.
    tau: embedding lag. int or List, if tau is an int then tauSource = tauTarget = tau.
         if tau is a list (len(tau) = 2) then tauSource = tau[0] and tauTarget = tau[1].
    u: time ahead.
    nn: number of nearest-neighbors.
    alpha: volume correction value.
    snorm: if True normalize input signals to zero mean and unitary std, then add small
           amplitude noise.
    vcor:  if True, perform volume correction.

Return:
    Tksg: Transfer entropy from source to target.

 refs:
 1. https://doi.org/10.1103/PhysRevE.69.066138
 2. arXiv:1411.2003
 3. DOI: 10.1007/978-3-642-54474-3_1
 4. The  transfer  entropy  estimator  for  the  SECOND  ksg-algorithm  was  found  in
 10.1007/978-3-642-54474-3_1.

"""

import numpy as np
from scipy.special import psi
from sklearn.neighbors import KDTree

try:
    from . import knnTools as knnT
except ImportError:
    import knnTools as knnT


def knnTent01(source,
              target,
              m=2,
              tau=1,
              u=1,
              nn=5,
              alpha=0.01,
              snorm=True,
              vcor=False):

    metric = "infinity"
    if snorm:
        # series normalization (zero mean and unitary std) and adding some noise
        source = knnT.series_normalization(source)
        target = knnT.series_normalization(target)
    else:
        source = knnT.toColVector(source)
        target = knnT.toColVector(target)

    # delay vectors
    taus, taut = knnT.parseTau(tau)

    sv = knnT.embedding(source, m, taus)
    tv = knnT.embedding(target, m, taut)
    N = min(sv.shape[0], tv.shape[0])
    sv = sv[:N, :]
    tv = tv[:N, :]

    # Form vector space V = {Target_(t+u), Target_t, Source_t}
    vec = np.concatenate((tv[u:, :], tv[:-u, :], sv[:-u, :]), axis=1)
    N, _ = vec.shape
    # Find the knn in the complete space
    tree = KDTree(vec, metric=metric)
    nn_inds = tree.query(vec,
                         k=nn + 1,
                         return_distance=False,
                         sort_results=True)
    dist = np.zeros(N)
    # Find distances in each marginal space and total volume
    for i in range(N):
        # d = np.abs(vec[nn_inds[i], :] - vec[i, :]).max()
        # dist[i] = d
        # vol[i] = np.log(d)
        dt = np.abs(vec[nn_inds[i], :] - vec[i, :]).max(axis=0)
        dist[i] = dt.max()  # distance for 1st-KSG-alg.

    dist = dist - 1e-15

    # Num. of nearest neighbors in space {Target_t}
    tree = KDTree(vec[:, m:2 * m], metric=metric)
    nnT = tree.query_radius(vec[:, m:2 * m],
                            dist,
                            return_distance=False,
                            count_only=True)
    # Num. of nearest neighbors in space {Target_(t+u), Target_t}
    tree = KDTree(vec[:, :2 * m], metric=metric)
    nnTu = tree.query_radius(vec[:, :2 * m],
                             dist,
                             return_distance=False,
                             count_only=True)

    # Num. of nearest neighbors in space {Target_t, Source_t}
    tree = KDTree(vec[:, m:], metric=metric)
    nnTS = tree.query_radius(vec[:, m:],
                             dist,
                             return_distance=False,
                             count_only=True)

    # Calculate ksg estimator (algorithm 1)
    Tksg = psi(nn) + np.mean(psi(nnT)) - np.mean(psi(nnTu)) - np.mean(
        psi(nnTS))

    # Volume correction
    if vcor:
        vol = np.zeros(N)
        vol_rec = np.zeros(N)
        for i in range(N):
            vol[i], vol_rec[i] = knnT.correctVolume(vec[nn_inds[i], :], nn)

        c_inds = vol_rec > vol + np.log(alpha)
        vol_rec[c_inds] = vol[c_inds]
        e = np.mean(vol - vol_rec)
        Tksg = Tksg + e
    return Tksg


def knnTent02(source,
              target,
              m=2,
              tau=1,
              u=1,
              nn=5,
              alpha=0.01,
              snorm=True,
              vcor=False):

    metric = "infinity"
    if snorm:
        # series normlization (cero mean and unitary std) and adding some noise
        source = knnT.series_normalization(source)
        target = knnT.series_normalization(target)
    else:
        source = knnT.toColVector(source)
        target = knnT.toColVector(target)

    # delay vectors
    taus, taut = knnT.parseTau(tau)

    sv = knnT.embedding(source, m, taus)
    tv = knnT.embedding(target, m, taut)
    N = min(sv.shape[0], tv.shape[0])
    sv = sv[:N, :]
    tv = tv[:N, :]

    # Form vector space vec = {Target_(t+u), Target_t, Source_t}
    vec = np.concatenate((tv[u:, :], tv[:-u, :], sv[:-u, :]), axis=1)
    N, _ = vec.shape
    # Find the knn in the complete space
    tree = KDTree(vec, metric=metric)
    nn_inds = tree.query(vec,
                         k=nn + 1,
                         return_distance=False,
                         sort_results=True)
    dist = np.zeros((N, 3))
    # Find distances in each marginal space and total volume
    for i in range(N):
        dt = np.abs(vec[nn_inds[i], :] - vec[i, :]).max(axis=0)
        # Dists. in the marginal space {Target_t}
        dist[i, 0] = dt[m:2 * m].max()
        # Dists. in the marginal space {Target_(t+u), Target_t}
        dist[i, 1] = dt[:2 * m].max()
        # Dists. in the marginal space {Target_t, Source_t}
        dist[i, 2] = dt[m:].max()

    dist = dist - 1e-15
    # Num. of nearest neighbors in space {Target_t}
    tree = KDTree(vec[:, m:2 * m], metric=metric)
    nnT = tree.query_radius(vec[:, :m],
                            dist[:, 0],
                            return_distance=False,
                            count_only=True)
    # Num. of nearest neighbors in space {Target_(t+u), Target_t}
    tree = KDTree(vec[:, :2 * m], metric=metric)
    nnTu = tree.query_radius(vec[:, :2 * m],
                             dist[:, 1],
                             return_distance=False,
                             count_only=True)
    # Num. of nearest neighbors in space {Target_t, Source_t}
    tree = KDTree(vec[:, m:], metric=metric)
    nnTS = tree.query_radius(vec[:, m:],
                             dist[:, 2],
                             return_distance=False,
                             count_only=True)
    # Calculate ksg estimator (algorithm 2)
    Tksg = (psi(nn) - 2.0 / nn + psi(nnT).mean() - psi(nnTu).mean() -
            psi(nnTS).mean() + (1.0 / nnTu).mean() + (1.0 / nnTS).mean())

    # Volume correction
    if vcor:
        vol = np.zeros(N)
        vol_rec = np.zeros(N)
        for i in range(N):
            vol[i], vol_rec[i] = knnT.correctVolume(vec[nn_inds[i], :], nn)

        c_inds = vol_rec > vol + np.log(alpha)
        vol_rec[c_inds] = vol[c_inds]
        e = np.mean(vol - vol_rec)
        Tksg = Tksg + e
    return Tksg
